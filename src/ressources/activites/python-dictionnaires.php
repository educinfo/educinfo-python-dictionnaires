<?php

register_activity('python-dictionnaires',array(
		'category'=>'NSI1',
		'section'=>'NSI1typeconstruit',
		'type'=>'url',
		'titre'=>'Introduction aux dictionnaires en python',
		'auteur'=>"Laurent COOPER",
		'URL'=>'index.php?page=python_dic_intro&activite=python-dictionnaires',
		'commentaire'=>"Le dictionnaire est un type construit en informatique qui est très utile pour travailler sur les données structurées.",
		'directory'=>'python-dictionnaires',
		'image'=>'assets/img/python-logo.png',
		'prerequis'=>NULL
	)
);