entree=""
depenses = []
numero = 1
while entree != "F":
    question = f"Entrez F pour arréter ou la somme payée par la personne n°{numero} : "
    entree=input(question)
    if entree != "F":
        depenses.append(float(entree))
        numero += 1

# Fin des entrées 
# On calcule tout l'argent dépensé et on fait la moyenne par personne
total = 0
for somme in depenses:
    total += somme
moyenne = total / len(depenses)

# On va maintenant dire combien doit chacun.
# Si c'est positif, on le rembourse.
# Si c'est négatif, il doit mettre dans le pot commun
for i in range(len(depenses)):
    difference = depenses[i] - moyenne
    if  difference >= 0:
        print(f"La personne n°{i+1}  doit recevoir : {difference:.2f}")
    else:
        print(f"La personne n°{i+1}  doit payer : {-difference:.2f}")