fruits = ['orange','ananas','kiwi','abricot']
fichier = open("fruits.txt",'w')
for fruit in fruits:
    fichier.write(fruit+'\n')
fichier.close()
print("Le fichier est enregistré")

print("\nlecture du fichier ligne par ligne avec readline\n")
fichier = open("fruits.txt")
# on lit ligne par ligne
ligne = fichier.readline()
while ligne !="":
    ligne = ligne.rstrip("\n") #on enlève le saut de ligne à la fin
    if ligne =="kiwi":
        print("j'adore le kiwi !")
    else:
        print(f"J'aime bien l'{ligne} mais je préfère le kiwi.")
    ligne = fichier.readline() # lecture de la ligne suivante
# on ne ferme qu'après avoir tout lu
fichier.close()