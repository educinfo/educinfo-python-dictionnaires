# Programme de carnet d'adresse

# Définition des fonctions 
def ajouter_contact():
    # Demande les infos à l'utilisateur
    # renvoie un dictionnaire
    infos = {}
    nom = input("Quel est son nom ?")
    # Compléter

    return infos

def afficher_contacts(liste):
    print("Liste des contacts : ")
    for contact in liste :
        # afficher les contacts de la liste
        pass

def afficher_un_contact(liste,numero):
    print(f"Affichage détaillé du contact n°{numero}")
    contact = liste[numero]
    print(f"Nom : {contact['Nom']}")
    # Écrire ici la suite du code

def supprimer_contact(liste,numero):
    del liste[numero]

####
# Programme principal
####
# Ici, création de la liste des contacts :
contacts = []

# Ici, création du premier contact. Complétez avec vos infos :
mes_infos={}
contacts.append(mes_infos)

afficher_contacts(contacts)
afficher_un_contact(contacts,0)

# Ajout de deux nouveaux contacts
for i in range(2):
    nouveau_contact = ajouter_contact()
    contacts.append(nouveau_contact)
    afficher_contacts(contacts)

# Suppression du premier contact ajouté
supprimer_contact(contacts,1)
afficher_contacts(contacts)

# Affichage des détails d'un contact
afficher_un_contact(contacts,1)
