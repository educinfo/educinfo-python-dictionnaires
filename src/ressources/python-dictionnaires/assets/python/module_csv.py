import csv

with open('contacts.csv','w',newline='') as csvfile:
    ecrivain = csv.writer(csvfile, delimiter=';',quotechar='"')
    ecrivain.writerow(['Albert','Enstein','USA'])
print("fichier enregistré")

print("Lecture du fichier\n--")
with open('contacts.csv', newline='') as csvfile:
    lecteur = csv.reader(csvfile, delimiter=';', quotechar='"')
    for ligne in lecteur:
        print(ligne)
print("--")

print("Modification du fichier")
with open('contacts.csv','a',newline='') as csvfile:
    ecrivain = csv.writer(csvfile, delimiter=';',quotechar='"')
    ecrivain.writerow(['Marie','Curie','France'])

print("Fichier modifié")

print("Lecture du fichier\n--")
with open('contacts.csv', newline='') as csvfile:
    lecteur = csv.reader(csvfile, delimiter=';', quotechar='"')
    for ligne in lecteur:
        print(ligne)
print("--")