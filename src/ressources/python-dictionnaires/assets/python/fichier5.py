fruits = ['orange','ananas','kiwi','abricot']
with open("fruits.txt",'w') as fichier :
    for fruit in fruits:
        fichier.write(fruit+'\n')

print("Le fichier est enregistré\n")

with open("fruits.txt") as fichier:
    for ligne in fichier:
        ligne = ligne.rstrip("\n")
        print(f"Nouveau fruit: {ligne}")

print("\nle fichier a été lu et affiché")