import csv

champs=['Prénom','Nom','Résidence']

Scientifiques = []

albert = {'Nom':'Einstein',
    'Prénom':'Albert',
    'Résidence':'USA'
}

marie = {'Prénom':'Marie',
    'Nom':'Curie',
    'Résidence':'France'
}


with open('contacts.csv','w',newline='') as csvfile:
    ecrivain = csv.DictWriter(csvfile,fieldnames=champs, delimiter=';',quotechar='"')
    ecrivain.writeheader()
    ecrivain.writerow(albert)
print("fichier enregistré")

print("Lecture du fichier\n--")
with open('contacts.csv', newline='') as csvfile:
    lecteur = csv.DictReader(csvfile, delimiter=';', quotechar='"')
    for ligne in lecteur:
        print("-")
        for k,v in ligne.items() :
            print(f"{k} : {v}")
print("--")

print("Modification du fichier")
with open('contacts.csv','a',newline='') as csvfile:
    ecrivain = csv.DictWriter(csvfile,fieldnames=champs, delimiter=';',quotechar='"')
    ecrivain.writerow(marie)

print("Fichier modifié")

print("Lecture du fichier\n--")
with open('contacts.csv', newline='') as csvfile:
    lecteur = csv.DictReader(csvfile, delimiter=';', quotechar='"')
    for ligne in lecteur:
        print("-")
        for k,v in ligne.items() :
            print(f"{k} : {v}")
print("--")