# Programme de carnet d'adresse

# Définition des fonctions 
def ajouter_contact():
    """ demande les infos à l'utilisateur.
        Renvoie un dictionnaire """
    pass

def afficher_contacts(liste):
    """ Affiche la liste de tous les contacts
        Ne renvoie rien """
    pass

def afficher_un_contact(liste,numero):
    """ Affiche un contact (par son numéro)
        Ne renvoie rien """
    pass

def supprimer_contact(liste,numero):
    """ Supprime un contact
        Ne renvoie rien"""
    pass

def sauvegarder_contacts(nom_fichier,liste):
    """ Sauvegarde tous les contacts de la liste dans le fichier de nom nom_fichier
        Ne renvoie rien """
    pass

# Programme principal
if __name__ == "__main__":
    liste = []
    reponse =""
    while reponse != "q":
        print("Entrez a pour ajouter un contact.")
        print("Entrez l pour lister les contacts.")
        print("Entrez d pour afficher le détail d'un contact.")
        print("Entrez s pour supprimer un contact.")
        print("Entrez q pour quitter (une sauvegarde des contacts sera faite).")
        reponse = input(" Que voulez vous faire ? ")

        if reponse == "a":
            contact = ajouter_contact()
            liste.append(contact)
        elif reponse == "l":
            afficher_contacts(liste)
        elif reponse =="d":
            numero = int(input("Donnez le numéro du contact à afficher : "))
            afficher_un_contact(liste,numero)
        elif reponse == "s":
            numero = int(input("Donnez le numéro du contact à supprimer : "))
            supprimer_contact(liste,numero)
        elif reponse == "q":
            nom_fichier=input("Donnez le nom du fichier de sauvegarde : ")
            sauvegarder_contacts(nom_fichier,liste)
        else :
            print("Je n'ai pas compris ...")