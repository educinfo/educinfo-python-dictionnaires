fruits = ['orange','ananas','kiwi','abricot']
fichier = open("fruits.txt",'w')
for fruit in fruits:
    fichier.write(fruit+'\n')
fichier.close()
print("Le fichier est enregistré")

print("\nLecture du fichier en une seule fois par read\n")
fichier = open("fruits.txt")
contenu = fichier.read()
# la variable contenu vaut pour notre exemple "orange\nananas\nkiwi\nabricot\n"
fichier.close()
print(contenu)
