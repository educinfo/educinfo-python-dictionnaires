fruits = ['orange','ananas','kiwi','abricot']
fichier = open("fruits.txt",'w')
for fruit in fruits:
    fichier.write(fruit+'\n')
fichier.close()
print("Le fichier est enregistré")

print("\nlecture du fichier en une fois par readlines\n")
fichier = open("fruits.txt")
lignes = fichier.readlines()
# la variable lignes vaut pour notre exemple ['orange\n','ananas\n','kiwi\n','abricot\n']
fichier.close()
for element in lignes:
    element = element.rstrip("\n") #on enlève le saut de ligne à la fin
    if element =="kiwi":
        print("j'adore le kiwi !")
    else:
        print(f"J'aime bien l'{element} mais je préfère le kiwi.")
