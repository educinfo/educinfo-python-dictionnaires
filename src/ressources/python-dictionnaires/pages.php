<?php
/*

	Pages des tutoriel d'apprentissage de p5js
*/
global $pages;

// Bases du javascript
$menu_activite = array(
    "titre" => "Introduction aux dictionnaires en python",
    "contenu" => [
        'python_dic_intro',
        'python_dic_creation',
        'python_dic_fichier',
        'python_dic_CSV',
        'python_module_CSV'
    ]
);

// pages des bases du javascript
$new_pages = array(
    'python_dic_intro' => array(
        "template" => 'python-dictionnaires/introduction.twig.html',
        "menu" => 'python-dictionnaires',
        'page_precedente' => NULL,
        'page_suivante' => 'python_dic_creation',
        'titre' => 'Le pique-nique'
    ),
    'python_dic_creation' => array(
        "template" => 'python-dictionnaires/creation_lecture.twig.html',
        "menu" => 'python-dictionnaires',
        'page_precedente' => 'python_dic_intro',
        'page_suivante' => 'python_dic_fichier',
        'titre' => "Garder le contact"
    ),
    'python_dic_fichier' => array(
        "template" => 'python-dictionnaires/fichiers.twig.html',
        "menu" => 'python-dictionnaires',
        'page_precedente' => 'python_dic_creation',
        'page_suivante' => 'python_dic_CSV',
        'titre' => "La mémoire"
    ),
    'python_dic_CSV' => array(
        "template" => 'python-dictionnaires/csv.twig.html',
        "menu" => 'python-dictionnaires',
        'page_precedente' => 'python_dic_fichier',
        'page_suivante' => 'python_module_CSV',
        'titre' => "Structurer la mémoire"
    ),
    'python_module_CSV' => array(
        "template" => 'python-dictionnaires/pycsv.twig.html',
        "menu" => 'python-dictionnaires',
        'page_precedente' => 'python_dic_CSV',
        'page_suivante' => NULL,
        'titre' => "Module CSV"
    ),
);

$pages = array_merge($pages, $new_pages);
